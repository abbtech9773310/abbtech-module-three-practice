package org.abbtech.practice.lesson4.homework;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "Book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable=false, updatable=false)
    private int bookID;

    @Column(name = "Title")
    private String title;

    @Column(name = "Author")
    private String author;

    @ManyToOne
    @JoinColumn(name = "BookID")
    private Professor professor;

}