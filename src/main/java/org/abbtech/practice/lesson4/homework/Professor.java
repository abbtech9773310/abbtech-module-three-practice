package org.abbtech.practice.lesson4.homework;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "Professor")
public class Professor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ProfessorID")
    private int professorID;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "DepartmentID")
    private Department department;

    @OneToMany(mappedBy = "professor", cascade = CascadeType.ALL)
    private List<Book> books;

}
