package org.abbtech.practice.lesson4.homework;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "StudentIDCard")
public class StudentIDCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CardID")
    private int cardID;

    @Column(name = "CardNumber")
    private String cardNumber;

    @Column(name = "ExpiryDate")
    private Date expiryDate;

    @OneToOne
    @JoinColumn(name = "CardID")
    private Student student;

}