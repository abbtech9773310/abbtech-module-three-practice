package org.abbtech.practice.module4.homework.repository;


import org.abbtech.practice.module4.homework.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
